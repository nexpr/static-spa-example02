const navigate = url => {
	fetch(url).then(e => e.text()).then(body => {
		const doc = new DOMParser().parseFromString(body, "text/html")
		if([...doc.scripts].some(e => e.type === "module" && e.getAttribute("src") === "/spa.js")) {
			document.head.innerHTML = ""
			document.head.append(...doc.head.childNodes)
			document.body.innerHTML = ""
			document.body.append(...doc.body.childNodes)
			history.pushState(null, "", url)
			loadPageModule()
		} else {
			location.href = a.href
		}
	}).catch(err => {
		console.error(err)
		location.href = a.href
	})
}

const loadPageModule = () => {
	const url = location.href
	const module_url = url.endsWith("/") ? url + "index.js" : url.replace(/(\.[^\/.]+)$/, ".js")
	import(module_url).then(({ render }) => render()).catch(console.error)
}

window.addEventListener("click", eve => {
	const a = eve.target.closest("a[href]")
	if(!a || !a.href.startsWith(location.origin)) return
	eve.preventDefault()
	navigate(a.href)
})

loadPageModule()

window.navigate = navigate
