const render = () => {
	document.querySelector("input").addEventListener("input", eve => {
		eve.target.nextElementSibling.textContent = eve.target.value
	})
}

export { render }
