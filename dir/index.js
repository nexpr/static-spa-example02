const render = () => {
	document.querySelector(".nums").addEventListener("input", eve => {
		const sum = Array.from(eve.currentTarget.querySelectorAll("input")).reduce((a, b) => a + b.valueAsNumber, 0)
		eve.currentTarget.nextElementSibling.textContent = sum
	})
}

export { render }
